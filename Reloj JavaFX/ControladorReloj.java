import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;

public class ControladorReloj {

    @FXML
    private Label lblMinutos;
    
    @FXML
    private BorderPane panel1;

    @FXML
    private Button cmdSegundos;

    @FXML
    private TextField txtSegundos;

    @FXML
    private Label lblSegundos;

    @FXML
    private Label lblTitulo;

    @FXML
    private Button cmdHoras;

    @FXML
    private Button cmdMinutos;

    @FXML
    private GridPane panelDatos;

    @FXML
    private Label lblHoras;

    @FXML
    private TextField txtMinutos;

    @FXML
    private ImageView RelojImage;

    @FXML
    private GridPane panelBotones;

    @FXML
    private TextField txtHoras;
    
    private ModeloReloj reloj;
    
    public ControladorReloj(){
    reloj = new ModeloReloj();
    }
    
    @FXML
    void moverSegundero() {
    reloj.moverSegundero();
    txtSegundos.setText(Integer.toString(reloj.getSegundos()));
    txtMinutos.setText(Integer.toString(reloj.getMinutos()));
    txtHoras.setText(Integer.toString(reloj.getHoras()));
    }

    @FXML
    void moverMinutero() {
    reloj.moverMinutero();
    txtMinutos.setText(Integer.toString(reloj.getMinutos()));
    txtHoras.setText(Integer.toString(reloj.getHoras()));
    }

    @FXML
    void moverHorario() {
    reloj.moverHorario();
    txtHoras.setText(Integer.toString(reloj.getHoras()));
    }

}
